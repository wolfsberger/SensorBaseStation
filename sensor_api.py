from flask import Flask
from flask import request
from flask import send_from_directory
from flask import g
from flask_socketio import SocketIO
from flask_socketio import send, emit
from app import app
from app import socketio
from database import *
import json

@app.route('/api/sensors/', methods=['GET'])
def api_getSensorInfo():
	return json.dumps({"Sensors": getSensors()}, indent=4)

@app.route('/api/sensors/', methods=['POST'])
def api_addSensor():
	jsonData = request.json
	if jsonData is not None:
		addSensor(jsonData['name'], jsonData['description'])
		socketio.emit('update_event', json.dumps({ "event_id": "new_sensor"}))
		return "TEST"
	return "{}"