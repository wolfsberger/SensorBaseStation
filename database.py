import sqlite3

def getDatabase():
	return sqlite3.connect('database.db')

def getSensors():
	connection = getDatabase()
	c = connection.cursor()
	createSensorTableIfNotExists(c)
	
	sensors = []
	for row in c.execute("SELECT * FROM Sensors"):
		sensors.append({'ID': row[0], 'name': row[1], 'description': row[2]})

	connection.commit()
	connection.close()
	return sensors

def addSensor(name, description):
	connection = getDatabase()
	c = connection.cursor()
	createSensorTableIfNotExists(c)

	c.execute("INSERT INTO Sensors ('name', 'description') VALUES (?, ?)", (name, description))

	connection.commit()
	connection.close()

def createSensorTableIfNotExists(c):
	c.execute("CREATE TABLE IF NOT EXISTS Sensors ( ID INTEGER PRIMARY KEY AUTOINCREMENT, name text NOT NULL, description text )")

