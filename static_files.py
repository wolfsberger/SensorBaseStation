from flask import Flask
from flask import send_from_directory
from app import app

@app.route('/')
def index():
    return send_from_directory('static', 'led.html')

@app.route('/static/<path:path>')
def static_files(path):
	print(path)
	return send_from_directory('static', path)
