from flask import Flask
from flask import request
from flask import send_from_directory
from flask import g
from flask_socketio import SocketIO
from flask_socketio import send, emit

import json

app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/api/led/<int:state>', methods=['POST'])
def control_led(state):

	led_state = getattr(g, 'led_state', None)
	success = False

	if (led_state is None):
		led_state = 0
		success = True

	if (state == 1):
		led_state = 1
		success = True

	if (state == 0):
		led_state = 0

	if (success):
		socketio.emit('update_event', json.dumps({ "event_id": "led_changed", "led_state": led_state }), broadcast=True)

	return json.dumps({ "success": success, "led_state": led_state })


@app.route('/api/led/', methods=['GET'])
def get_led():
	led_state = getattr(g, 'led_state', None)	
	if (led_state is None):
		led_state = 0

	return json.dumps({ "success": True, "led_state": 1 })

@app.route('/api/notify/<msg>')
def notify_all(msg):
	print(msg)
	socketio.emit('update_event', json.dumps({ "event_id": "alert", "alert_text": msg }))
	return "Notify sent"
	
import static_files
import sensor_api

if __name__ == "__main__":
	app.run()
